FROM node:16

ARG NODE_ENV=production
ENV NODE_ENV $NODE_ENV
EXPOSE 8001

WORKDIR /home/node
COPY ./package.json .
COPY ./package-lock.json .
RUN npm ci
RUN npm cache clean --force
COPY . .
RUN chmod 770 -R /home/node
RUN chown node:node -R /home/node
USER node
RUN npm run build
CMD ["node", "./index.js"]
