import { Pool, PoolClient } from 'pg';

export const pg: Pool = new Pool({
  connectionString: process.env.PG_STRING || 'postgres://e2t:e2t@localhost:5432/e2t?ssldisable=true',
});

export const getConnection = async (): Promise<PoolClient> => {
  return await pg.connect();
};

export const fromTwitchId = async (id: string) => {
  let client: PoolClient|undefined;
  try {
    client = await getConnection();
    const result = await client.query(findPlayerById, [id]);
    if (result.rowCount < 1) {
      return null;
    }
    return result.rows[0];
  } finally {
    if (client) {
      client.release();
    }
  }
};

export const findPlayerById = `SELECT ign, login, display FROM players WHERE id = $1 LIMIT 1;`;
