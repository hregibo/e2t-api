import { SIGKILL, SIGQUIT, SIGSTOP, SIGTERM } from 'constants';
import Koa from 'koa';
import KoaBodyparser from 'koa-bodyparser';
import KoaRouter from 'koa-router';
import * as DB from './db';

export const server = new Koa();
server.use(KoaBodyparser());
export const router = new KoaRouter();

router.get('/twitch/id/:tid', async (ctx, next) => {
  const tid = ctx.params.tid;
  try {
    const foundUsername = await DB.fromTwitchId(tid);
    if (foundUsername) {
      ctx.body = JSON.stringify(foundUsername);
      ctx.status = 200;
      ctx.set('content-type', 'application/json');
      return next();
    }
  } catch (err) {
    console.log('Failure on fromTwitchId', err);
    ctx.status = 500;
    ctx.body = JSON.stringify({
      error: 500,
      message: 'Something went wrong when fetching the EVE Character.',
    });
    ctx.set('content-type', 'application/json');
    return next();
  }
  ctx.status = 404;
  ctx.set('content-type', 'application/json');
  ctx.body = JSON.stringify({error: 404, message: 'IGN Not found'});
  return next();
});

process.on('SIGINT', () => {
  console.log('exiting due to received signal');
  process.exit(0);
});

process.on('SIGTERM', () => {
  console.log('exiting due to received signal');
  process.exit(0);
});

// final instruction set for routes
server.use(router.routes());
server.use(router.allowedMethods());
server.listen(8001, '0.0.0.0');
